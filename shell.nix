with import <nixpkgs> {}; mkShell {
  packages = [
    cargo
    rustc
    rust-analyzer-unwrapped
    rustfmt
  ];
  env.RUST_SRC_PATH = "${rustPlatform.rustLibSrc}";
}
