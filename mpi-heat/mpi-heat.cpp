#include <climits>
#include <cstdint>
#include <fmt/core.h>
#include <mpi.h>
#include <numeric>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>

using namespace fmt;

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);
  auto world = MPI_COMM_WORLD;

  int processRank;
  int sizeCluster;
  MPI_Comm_size(world, &sizeCluster);
  MPI_Comm_rank(world, &processRank);

  char *hostname = (char *)calloc(HOST_NAME_MAX, sizeof(char));
  gethostname(hostname, HOST_NAME_MAX);

  for (auto i = 0; i < sizeCluster; i++) {
    if (i == processRank) {
      auto pid = getpid();
      println("{}/{}> pid: {}\thost: {}", processRank + 1,
              sizeCluster, pid, hostname);
    }
    MPI_Barrier(world);
  }

  // Heat eq

  std::vector<float> x;
  std::fill(x.begin(), x.end(), 1);

  MPI_Finalize();
  println("Goodbye");
  return EXIT_SUCCESS;
}