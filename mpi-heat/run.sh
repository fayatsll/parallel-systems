#! /usr/bin/env -S guix shell -m manifest.scm -- bash
set -eux

printenv PWD
which -a mpirun

mpirun -np 4 --hostfile $OAR_NODE_FILE mpi-heat
