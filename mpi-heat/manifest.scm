(use-modules (guix)
             (gnu packages)
             (gnu packages base))


(specifications->manifest (list
                            "gcc-toolchain"
                            "cmake"
                            "fmt"
                            "openmpi"
                            "make"
                            ))
