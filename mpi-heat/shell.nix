with import <nixpkgs> {};
  mkShell {
    packages = [
      openmpi
      fmt

      cmake
      pkg-config

      clang-tools
    ];

    hardeningDisable = ["all"];
  }
