use std::{sync::mpsc, thread};

fn main() {
    let (tx, rx) = mpsc::channel();

    for t in u64::try_from(0).unwrap()..100 {
        let mytx = tx.clone();
        thread::spawn(move || {
            // println!("Starting thread {t}");
            let result = t.pow(2);
            mytx.send(result).unwrap();
            // println!("Goodbye thread {t}");
        });
    }

    let mut accumulator: u64 = 0;

    drop(tx);

    loop {
        match rx.recv() {
            Err(_) => {
                println!("Goodbye");
                break;
            }
            Ok(next) => {
                accumulator += next;
            }
        }
    }

    println!("res={accumulator}");
}
