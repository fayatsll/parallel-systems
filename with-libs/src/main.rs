use rayon::{prelude::*, ThreadBuilder, ThreadPoolBuilder};

fn main() {
    let input = u64::try_from(1).unwrap()..100;

    ThreadPoolBuilder::new()
        .num_threads(4)
        .build()
        .unwrap()
        .install(|| {
            let res = input.into_par_iter().map(|elem| elem.pow(2)).reduce(
                || 0,
                |a, b| {
                    let th = std::thread::current();
                    println!("th: {th:?}");
                    a + b
                },
            );
            println!("res={res}");
        });
}
